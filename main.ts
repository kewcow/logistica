const sum = (a: number,b: number) => {
  const primaryNumber = a;
  const secondNumber = b;
  return primaryNumber + secondNumber;
}

const rest = (a: number, b: number) => {
  const primaryNumber = a;
  const secondNumber = b;
  return primaryNumber - secondNumber;
}

// TESTING FUNCTION SIMPLE
console.log(sum(1,2)) // 4

rest(1,2) // 1
